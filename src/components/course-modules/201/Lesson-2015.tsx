import LessonLayout from "@/src/components/lms/Lesson/LessonLayout";
import LessonIntroAndVideo from "@/src/components/lms/Lesson/LessonIntroAndVideo";

import module from "./module201.json";
import Docs2015 from "@/src/components/course-modules/201/Docs2015.mdx";

export default function Lesson2015() {
  const slug = "2015";
  const lessonDetails = module.lessons.find((lesson) => lesson.slug === slug);

  return (
    <LessonLayout moduleNumber={201} sltId="201.5" slug={slug}>
      <LessonIntroAndVideo lessonData={lessonDetails} />
      <Docs2015 />
    </LessonLayout>
  );
}
