import { PPBLContext } from "@/src/context/PPBLContext";
import { METADATA_KEY_QUERY } from "@/src/data/queries/metadataQueries";
import { TREASURY_UTXO_QUERY } from "@/src/data/queries/treasuryQueries";
import { faucetRegistryMetadata } from "@/src/types/faucet";
import { useQuery, gql, useLazyQuery } from "@apollo/client";
import {
  Box,
  Center,
  Divider,
  Flex,
  Grid,
  Heading,
  Spacer,
  Spinner,
  Stack,
  Text,
  useDisclosure,
  useToast,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Button,
  Accordion,
  AccordionItem,
  AccordionButton,
  AccordionIcon,
  AccordionPanel,
} from "@chakra-ui/react";
import {
  Data,
  resolvePaymentKeyHash,
  Transaction,
  UTxO,
  Asset,
  resolveScriptRef,
  PlutusScript,
  Action,
} from "@meshsdk/core";
import { useAddress, useWallet } from "@meshsdk/react";
import { useContext, useEffect, useState } from "react";
import faucetProject from "@/src/cardano/faucet-mini-project/faucets-plutus.json";
import { stringToHex } from "@/src/utils";

interface FaucetContractList {
  [key: string]: string;
}

const faucetContractList: FaucetContractList = faucetProject.faucets;

const FaucetTestInstance = () => {
  // ---------------------------------------------------------------------------------------
  // Depends on metadata registry + git merge to project
  //
  // Submit a merge request adding a line to faucets-plutus.json
  // ---------------------------------------------------------------------------------------
  const contractAddress =
    "addr_test1wrfjp0t656rpm8wq67mh5zy7kjhw7qz82l82xjka2lzmlacjn0aeg";

  const faucetPolicyId = "a4af431031b91e9130aa6b920c3b8b5c18befeb79e9e16d473205396";
  const faucetTokenName = "tScaffold";
  const faucetAsset = faucetPolicyId + stringToHex(faucetTokenName);
  const withdrawalAmount = 100;
  const refUTxOHash = "4dd9ed21500f44da9b8e520be3f158a97672a904f0dab7a17b2eccba05a6a350";
  const refUTxOIx = 0;
  const aboutToken = [];

  const outgoingDatum: Data = {
    alternative: 0,
    fields: [100, faucetTokenName],
  };

  const _faucetPlutusScript: PlutusScript = {
    version: "V2",
    code: faucetContractList[contractAddress],
  };

  const referenceUTxO: UTxO = {
    input: {
      outputIndex: refUTxOIx,
      txHash: refUTxOHash,
    },
    // query for refUTxO address and lovelace amount
    output: {
      address:
        "addr_test1qrw445926m0ffzme6fk65mq0d9004f66zyj6jtzlyv73t9sefs0xl34s9kau3can6zs9f2hyqe3sv5vnleytzxlkp5cqlym77s",
      amount: [{ unit: "lovelace", quantity: "17511530" }],
      scriptRef: resolveScriptRef(_faucetPlutusScript),
    },
  };

  // ---------------------------------------------------------------------------------------
  // Template:
  // ---------------------------------------------------------------------------------------

  const { connected, wallet } = useWallet();
  const address = useAddress();
  const [connectedPkh, setConnectedPkh] = useState<string | undefined>(undefined);
  const [redeemer, setRedeemer] = useState<Partial<Action> | undefined>(undefined);

  const ppblContext = useContext(PPBLContext);

  // Faucet Hooks:
  const [faucetBalance, setFaucetBalance] = useState<string | undefined>(undefined);
  const [newFaucetBalance, setNewFaucetBalance] = useState<string | undefined>(undefined);
  const [faucetAssetAtFaucetContract, setFaucetAssetAtFaucetContract] = useState<Asset[] | undefined>(undefined);
  const [faucetAssetToFaucetContract, setFaucetAssetToFaucetContract] = useState<Asset[] | undefined>(undefined);
  const [faucetAssetToBrowserWallet, setFaucetAssetToBrowserWallet] = useState<Asset[] | undefined>(undefined);
  const [numFaucetUTxOs, setNumFaucetUTxOs] = useState(0);
  const [inputFaucetUTxO, setInputFaucetUTxO] = useState<UTxO | undefined>(undefined);
  const [outputFaucetUTxO, setOutputFaucetUTxO] = useState<Partial<UTxO> | undefined>(undefined);

  const [successTxHash, setSuccessTxHash] = useState<string | undefined>(undefined);

  // For Chakra Modal:
  const { isOpen: isConfirmationOpen, onOpen: onConfirmationOpen, onClose: onConfirmationClose } = useDisclosure();
  const { isOpen: isSuccessOpen, onOpen: onSuccessOpen, onClose: onSuccessClose } = useDisclosure();
  const toast = useToast();

  const {
    data: faucetQueryData,
    loading: faucetQueryLoading,
    error: faucetQueryError,
  } = useQuery(TREASURY_UTXO_QUERY, {
    variables: {
      contractAddress: contractAddress,
    },
  });

  useEffect(() => {
    if (address) {
      setConnectedPkh(resolvePaymentKeyHash(address));
    }
  }, [address]);

  useEffect(() => {
    if (connectedPkh && ppblContext) {
      const _redeemer: Partial<Action> = {
        data: {
          alternative: 0,
          fields: [connectedPkh, "222" + ppblContext.connectedContribToken],
        },
      };
      setRedeemer(_redeemer);
    }
  }, [connectedPkh, ppblContext]);

  useEffect(() => {
    if (faucetQueryData) {
      setNumFaucetUTxOs(faucetQueryData.utxos?.length);
    }

    if (faucetQueryData && faucetQueryData.utxos && faucetQueryData.utxos.length > 0) {
      const _tokenBalance = faucetQueryData.utxos[0].tokens[0].quantity;
      const _newTokenBalance = parseInt(_tokenBalance) - withdrawalAmount;
      setFaucetBalance(_tokenBalance);
      setNewFaucetBalance(_newTokenBalance.toString());
    }
  }, [faucetQueryData]);

  useEffect(() => {
    if (faucetBalance && newFaucetBalance) {
      const _faucetAssetToBrowserWallet: Asset[] = [
        { unit: "lovelace", quantity: "2000000" },
        {
          unit: faucetAsset,
          quantity: "100",
        },
      ];
      const _faucetAssetAtFaucetContract: Asset[] = [
        { unit: "lovelace", quantity: "2000000" },
        {
          unit: faucetAsset,
          quantity: faucetBalance,
        },
      ];
      const _faucetAssetToFaucetContract: Asset[] = [
        { unit: "lovelace", quantity: "2000000" },
        {
          unit: faucetAsset,
          quantity: newFaucetBalance,
        },
      ];

      setFaucetAssetToBrowserWallet(_faucetAssetToBrowserWallet);
      setFaucetAssetAtFaucetContract(_faucetAssetAtFaucetContract);
      setFaucetAssetToFaucetContract(_faucetAssetToFaucetContract);
    }
  }, [faucetBalance, newFaucetBalance]);

  useEffect(() => {
    if (
      faucetAssetAtFaucetContract &&
      faucetAssetToFaucetContract &&
      faucetQueryData.utxos &&
      faucetQueryData.utxos.length > 0
    ) {
      const _inputFaucetUTxO: UTxO = {
        input: {
          txHash: faucetQueryData.utxos[0].txHash,
          outputIndex: faucetQueryData.utxos[0].index,
        },
        output: {
          address: contractAddress,
          amount: faucetAssetAtFaucetContract,
          plutusData: faucetQueryData.utxos[0].datum.bytes,
        },
      };
      const _outputFaucetUTxO: Partial<UTxO> = {
        output: {
          address: contractAddress,
          amount: faucetAssetToFaucetContract,
        },
      };

      setInputFaucetUTxO(_inputFaucetUTxO);
      setOutputFaucetUTxO(_outputFaucetUTxO);
    }
  }, [faucetAssetAtFaucetContract, faucetAssetToFaucetContract]);

  // ---------------------------------------------------------------------------------------
  if (faucetQueryLoading) {
    return (
      <Center p="10">
        <Spinner size="xl" speed="1.0s" />
      </Center>
    );
  }

  if (faucetQueryError) {
    console.error(faucetQueryError);
    return (
      <Center flexDirection="column">
        <Heading size="lg">Error loading data...</Heading>;<pre>{JSON.stringify(faucetQueryError, null, 2)}</pre>
      </Center>
    );
  }

  const handleFaucetTx = async () => {
    try {
      console.log("click!");
      if (address && faucetAssetToBrowserWallet && inputFaucetUTxO && outputFaucetUTxO) {
        const tx = new Transaction({ initiator: wallet })
          .redeemValue({
            value: inputFaucetUTxO,
            script: referenceUTxO,
            datum: inputFaucetUTxO,
            redeemer: redeemer,
          })
          .sendValue(
            {
              address: contractAddress,
              datum: {
                value: outgoingDatum,
                inline: true,
              },
            },
            outputFaucetUTxO
          )
          .sendAssets(address, faucetAssetToBrowserWallet)
          .sendAssets(address, [
            { unit: "lovelace", quantity: "2000000" },
            { unit: ppblContext.connectedContribTokenUnit, quantity: "1" },
          ]);

        console.log("Your Tx: ", tx);
        const unsignedTx = await tx.build();
        const signedTx = await wallet.signTx(unsignedTx, true);
        const txHash = await wallet.submitTx(signedTx);
        console.log(txHash);
        onConfirmationClose();
        onSuccessOpen();
        setSuccessTxHash(txHash);
      }
    } catch (error: any) {
      if (error.info) {
        alert(error.info);
        console.log(error.info);
        console.log(error);
      } else {
        console.log(error);
      }
    }
  };

  return (
    <>
      <Box p="4" my="3" border="1px" borderColor="theme.light" borderRadius="lg" bg="purple.900" color="blue.100">
        <Heading>Test Txs</Heading>
        <Text>Faucet UTxOs: {numFaucetUTxOs}</Text>
        <Button mb="5" onClick={onConfirmationOpen} size="md">
          Build a Tx!
        </Button>
        <Divider />
        <Accordion allowMultiple>
          <AccordionItem>
            <AccordionButton>
              <Heading size="sm">Redeemer</Heading>
              <AccordionIcon />
            </AccordionButton>
            <AccordionPanel pb={4}>
              <pre>{JSON.stringify(redeemer, null, 2)}</pre>
            </AccordionPanel>
          </AccordionItem>
          <AccordionItem>
            <AccordionButton>
              <Heading size="sm">Faucet UTxO</Heading>
              <AccordionIcon />
            </AccordionButton>
            <AccordionPanel pb={4}>
              <Text>Input</Text>
              <pre>{JSON.stringify(inputFaucetUTxO, null, 2)}</pre>
              <Text>Output</Text>
              <pre>{JSON.stringify(outputFaucetUTxO, null, 2)}</pre>
            </AccordionPanel>
          </AccordionItem>
          <AccordionItem>
            <AccordionButton>
              <Heading size="sm">Reference UTxO</Heading>
              <AccordionIcon />
            </AccordionButton>
            <AccordionPanel pb={4}>
              <pre>{JSON.stringify(referenceUTxO, null, 2)}</pre>
            </AccordionPanel>
          </AccordionItem>
          <AccordionItem>
            <AccordionButton>
              <Heading size="sm">Datum</Heading>
              <AccordionIcon />
            </AccordionButton>

            <AccordionPanel pb={4}>
              <pre>{JSON.stringify(outgoingDatum, null, 2)}</pre>
            </AccordionPanel>
          </AccordionItem>
        </Accordion>
      </Box>
      <Modal blockScrollOnMount={false} isOpen={isConfirmationOpen} onClose={onConfirmationClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Confirmation Faucet Withdrawal</ModalHeader>
          <ModalBody></ModalBody>

          <ModalFooter>
            <Spacer />
            <Button bg="white" color="gray.700" onClick={handleFaucetTx}>
              Confirm
            </Button>
            <Button bg="white" color="gray.700" onClick={onConfirmationClose}>
              Close
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
      <Modal blockScrollOnMount={false} isOpen={isSuccessOpen} onClose={onSuccessClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Success!</ModalHeader>
          <ModalBody>
            <Text py="2">Transaction ID: {successTxHash}</Text>
            <Text py="2">It may take a few minutes for this tx to show up on a blockchain explorer.</Text>
          </ModalBody>
          <ModalFooter>
            <Button bg="white" color="gray.700" onClick={onSuccessClose}>
              Close
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};

export default FaucetTestInstance;
